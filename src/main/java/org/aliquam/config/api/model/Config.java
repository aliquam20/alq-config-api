package org.aliquam.config.api.model;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aliquam.cum.AlqJson;

@Data
@NoArgsConstructor
public class Config {
    private String key;
    private String raw;
    private boolean internal;

    public Config(String key, Object value) {
        this(key, value, true);
    }

    public Config(String key, Object value, boolean internal) {
        this.key = key;
        this.raw = AlqJson.serialize(value);
        this.internal = internal;
    }

    public <T> T getValue(Class<T> ref) {
        return AlqJson.parse(raw, ref);
    }

    public <T> T getValue(TypeReference<T> ref) {
        return AlqJson.parse(raw, ref);
    }

    public void setValue(Object value) {
        setRaw(AlqJson.serialize(value));
    }
}
