package org.aliquam.config.api.connector;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.concurrent.ConcurrentHashMap;

public class CachedAlqConfigConnector {

    private final ConcurrentHashMap<String, Object> cache = new ConcurrentHashMap<>();
    private final AlqConfigConnector connector;

    public CachedAlqConfigConnector(AlqConfigConnector connector) {
        this.connector = connector;
    }

    public void clear() {
        // TODO: clear on queue message
        cache.clear();
    }

    public <T> T get(String key, Class<T> ref) {
        //noinspection unchecked
        return (T) cache.computeIfAbsent(key, kkk -> connector.get(key, ref));
    }

    public <T> T get(String key, TypeReference<T> ref) {
        //noinspection unchecked
        return (T) cache.computeIfAbsent(key, kkk -> connector.get(key, ref));
    }

    public <T> T getOrSet(String key, T defaultValue, Class<T> ref) {
        return getOrSet(key, defaultValue, ref, true);
    }

    public <T> T getOrSet(String key, T defaultValue, TypeReference<T> ref) {
        return getOrSet(key, defaultValue, ref, true);
    }

    public <T> T getOrSet(String key, T defaultValue, Class<T> ref, boolean internal) {
        //noinspection unchecked
        return (T) cache.computeIfAbsent(key, kkk -> connector.getOrSet(key, defaultValue, ref, internal));
    }

    public <T> T getOrSet(String key, T defaultValue, TypeReference<T> ref, boolean internal) {
        //noinspection unchecked
        return (T) cache.computeIfAbsent(key, kkk -> connector.getOrSet(key, defaultValue, ref, internal));
    }

}
