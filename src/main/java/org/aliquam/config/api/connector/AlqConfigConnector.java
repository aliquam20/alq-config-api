package org.aliquam.config.api.connector;

import com.fasterxml.jackson.core.type.TypeReference;
import org.aliquam.config.api.model.Config;
import org.aliquam.cum.AlqJson;
import org.aliquam.cum.network.AlqConnector;
import org.aliquam.cum.network.AlqRequestBuilder;
import org.aliquam.cum.network.AlqUrl;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.session.api.connector.AlqSessionManager;

import java.util.List;

public class AlqConfigConnector {
    private static final AlqUrl alqConfigService = AlqConnector.of("config");
    private static final AlqUrl configResource = alqConfigService.resolve("config");

    private final AlqSessionManager sessionManager;

    public AlqConfigConnector(AlqSessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public List<Config> list() {
        return new AlqRequestBuilder(configResource)
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<Config>>() {});
    }

    public <T> T get(String key, Class<T> ref) {
        String raw = getRaw(key);
        return AlqJson.parse(raw, ref);
    }

    public <T> T get(String key, TypeReference<T> ref) {
        String raw = getRaw(key);
        return AlqJson.parse(raw, ref);
    }

    private String getRaw(String key) {
        return new AlqRequestBuilder(configResource.resolve(key))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(Config.class)
                .getRaw();
    }

    public void set(String key, Object value) {
        set(key, value, true);
    }

    public void set(String key, Object value, boolean internal) {
        set(new Config(key, value, internal));
    }

    public void set(Config config) {
        new AlqRequestBuilder(configResource.resolve(config.getKey()))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .PUT(config)
                .build().execute();
    }

    public <T> T getOrSet(String key, T defaultValue, Class<T> ref) {
        return getOrSet(key, defaultValue, ref, true);
    }

    public <T> T getOrSet(String key, T defaultValue, TypeReference<T> ref) {
        return getOrSet(key, defaultValue, ref, true);
    }

    public <T> T getOrSet(String key, T defaultValue, Class<T> ref, boolean internal) {
        try {
            return get(key, ref);
        } catch (AlqNotFoundResponseException nfe) {
            set(key, defaultValue, internal);
            return defaultValue;
        }
    }

    public <T> T getOrSet(String key, T defaultValue, TypeReference<T> ref, boolean internal) {
        try {
            return get(key, ref);
        } catch (AlqNotFoundResponseException nfe) {
            set(key, defaultValue, internal);
            return defaultValue;
        }
    }

    public void delete(Config config) {
        delete(config.getKey());
    }

    public void delete(String key) {
        new AlqRequestBuilder(configResource.resolve(key))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .DELETE()
                .build().execute();
    }
}
