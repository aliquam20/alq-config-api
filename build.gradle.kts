import org.aliquam.AlqUtils

plugins {
    `maven-publish`
    `java-library`
    id("org.sonarqube") version "3.0"
    jacoco
    id("io.freefair.lombok") version "6.0.0-m2"
    id("org.aliquam.alq-gradle-parent") version "0.4.14"
}
val alq = AlqUtils(project).withStandardProjectSetup()

group = "org.aliquam"
val artifactId = "alq-config-api"
val baseVersion = "0.0.1"
version = alq.getSemVersion(baseVersion)

val branchName: String? = System.getenv("BRANCH_NAME")
val isJenkins = branchName != null
val dockerRegistryHost = if (isJenkins) alq.getEnvOrPropertyOrThrow("DOCKER_REGISTRY_HOST") else null
val dockerImageName = "aliquam/${artifactId}"

java.sourceCompatibility = JavaVersion.VERSION_11

dependencies {
    implementation("org.aliquam:alq-cum:0.0.1-DEV_BUILD")
    implementation("org.aliquam:alq-session-api:0.0.1-DEV_BUILD")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.0") // Needed to use TypeReference<>
}

sonarqube {
    if (isJenkins) {
        properties {
            property("sonar.projectKey", "aliquam20_${artifactId}")
            property("sonar.organization", "aliquam")
            property("sonar.host.url", "https://sonarcloud.io")
            property("sonar.branch.name", branchName!!)
            property("sonar.coverage.jacoco.xmlReportPaths", "$projectDir/build/reports/jacoco/test/jacocoTestReport.xml")
        }
    }
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        csv.isEnabled = false
    }
    dependsOn(tasks.test) // tests are required to run before generating the report
}

tasks.withType<Test> {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("myPublication") {
            groupId = "$group"
            artifactId = artifactId
            from(components["java"])
        }
    }
}
